package com.trackit.main;

import org.json.JSONException;
import org.json.JSONObject;

import com.trackit.main.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class SplashScreen extends Activity{

	private static int SPLASH_TIME_OUT = 1000;
	private static int SPLASH_TIME_OUT2 = 500;
	private ImageView mScanner;
	private LinearLayout mScanner2;
	private Button button;
	private String number, email, password;
	private String regex = "[0-9]+";
	private Animation mAnimation, mAnimation2;
	final String PREFS_NAME = "TrackPrefsFile";
	private Boolean SUCCESS = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
		
		final SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		//TESTING!!!!!
		//settings.edit().putBoolean("my_first_time", true).commit(); 
		
		final EditText numberTxt = (EditText) findViewById(R.id.txtPhone);
		final EditText emailTxt = (EditText) findViewById(R.id.txtEmail);
		final EditText passwordTxt = (EditText) findViewById(R.id.txtPassword);
		button=(Button)findViewById(R.id.btnLogin);
		mScanner2=(LinearLayout)findViewById(R.id.formLogin);
		mScanner2.setVisibility(View.GONE);
		
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				
				if (settings.getBoolean("my_first_time", true)) {
					
					mScanner=(ImageView)findViewById(R.id.imgLogo);
					mScanner.setVisibility(View.VISIBLE);
			        mAnimation = new TranslateAnimation(0, 0, 0, -430);
			        mAnimation.setDuration(400);
			        mAnimation.setFillAfter(true);
			        mAnimation.setRepeatCount(0);
			        mAnimation.setRepeatMode(Animation.REVERSE);
			        mAnimation2 = new AlphaAnimation(0, 1);
			        mAnimation2.setDuration(1100);
			        mAnimation2.setFillAfter(true);
			        mScanner2.setVisibility(View.VISIBLE);
			        mScanner2.setAnimation(mAnimation2);
			        mScanner.setAnimation(mAnimation);
			        mScanner.startAnimation(mAnimation);
			        mScanner2.startAnimation(mAnimation2);
			        
				} else {
					
					HomeFragment.mPhoneNumber = settings.getString("phoneNo", " "); 
					Intent i = new Intent(SplashScreen.this, MainActivity.class);
					startActivity(i);
					finish();
					
				}
			}
		}, SPLASH_TIME_OUT);
			
		button.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				Context context = getApplicationContext();
				number =  numberTxt.getText().toString();
				email =  emailTxt.getText().toString();
				password =  passwordTxt.getText().toString();
				if (number.equals("")) {
					
					Toast toast = Toast.makeText(context, "Please enter your number", Toast.LENGTH_SHORT);
					toast.show();
					
				} else if (email.equals("")) {
					
					Toast toast = Toast.makeText(context, "Please enter your email", Toast.LENGTH_SHORT);
					toast.show();
					
				} else if (password.equals("")) {
					
					Toast toast = Toast.makeText(context, "Please enter your password", Toast.LENGTH_SHORT);
					toast.show();
					
				} else if (!number.matches(regex)) {
					
					Toast toast = Toast.makeText(context, "Please enter correct number", Toast.LENGTH_SHORT);
					toast.show();
					
				} else if (!email.contains("@") || !email.contains(".")) {
					
					Toast toast = Toast.makeText(context, "Please enter correct email", Toast.LENGTH_SHORT);
					toast.show();
					
				} else {
					
			    	JSONObject jsonObjSend = new JSONObject();
			        try {
			        		//Add the phone number, latitude and longitude to the JSON object
			                jsonObjSend.put("PhoneNo", number); 
			                jsonObjSend.put("Email", email);
			                
			        } catch (JSONException e) {
			                e.printStackTrace();
			        }
			        // Send the HttpPostRequest and receive a JSONObject in return
			        JSONObject jsonObjRecv = MyHttpClient.SendHttpPost("http://trackitwebapi.apphb.com/api/user/registerUser", jsonObjSend);
			     
					//parse the properties of the json object
					try {
						if (jsonObjRecv != null && jsonObjRecv.getString("message").equals("fail")) {
							
							Toast toast = Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT);
							toast.show();
							
						} else {
							
							SUCCESS = true;
							
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			        
					if (SUCCESS) {
						
				        mAnimation = new TranslateAnimation(0, 0, -430, 0);
				        mAnimation.setDuration(400);
				        mAnimation.setFillAfter(true);
				        mAnimation.setRepeatCount(0);
				        mAnimation.setRepeatMode(Animation.REVERSE);
				        mAnimation2 = new AlphaAnimation(1, 0);
				        mAnimation2.setDuration(80);
				        mAnimation2.setFillAfter(true);
				        mScanner2.setAnimation(mAnimation2);
				        mScanner.setAnimation(mAnimation);
				        mScanner.startAnimation(mAnimation);
				        mScanner2.startAnimation(mAnimation2);	
				        
						new Handler().postDelayed(new Runnable() {
			
							@Override
							public void run() {
								
				    			HomeFragment.mPhoneNumber = number; 
				                settings.edit().putString("phoneNo", number).commit();
								settings.edit().putBoolean("my_first_time", false).commit(); 
								
								Intent i = new Intent(SplashScreen.this, MainActivity.class);
								startActivity(i);
			
								finish();
							}
						}, SPLASH_TIME_OUT2);
					}
				}
			}
		});
		
	}

}
