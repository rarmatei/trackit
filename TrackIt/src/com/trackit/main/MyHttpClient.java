/*
 * (c) public domain: http://senior.ceng.metu.edu.tr/2009/praeda/2009/01/11/a-simple-restful-client-at-android/
 */
package com.trackit.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import android.util.Log;

public class MyHttpClient {
        private static final String TAG = "HttpClient";

        /*
         * Posts a json object to the specified URL.
         */
        public static JSONObject SendHttpPost(String URL, JSONObject jsonObjSend) {

                try {
                        DefaultHttpClient httpclient = new DefaultHttpClient();
                        HttpPost httpPostRequest = new HttpPost(URL);

                        StringEntity se;
                        se = new StringEntity(jsonObjSend.toString());
                        //set header parameters
                        httpPostRequest.setEntity(se);
                        httpPostRequest.setHeader("Accept", "application/json");
                        //important parameter as we're telling which type of object we're sending
                        httpPostRequest.setHeader("Content-type", "application/json");
                        httpPostRequest.setHeader("Accept-Encoding", "gzip");

                        long t = System.currentTimeMillis();
                        HttpResponse response = httpclient.execute(httpPostRequest);

                        HttpEntity entity = response.getEntity();
                        //if we got a response
                        if (entity != null) {
                                InputStream instream = entity.getContent();
                                //set the encoding
                                Header contentEncoding = response.getFirstHeader("Content-Encoding");
                                if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                                        instream = new GZIPInputStream(instream);
                                }
                                //parse the stream
                                String resultString= convertStreamToString(instream);
                                instream.close();
                                //because the returned object will be surrounded by "[..]"
                                //which represents a JSON array, we need to convert to an object
                                //so we remove them
                                //resultString = resultString.substring(1,resultString.length()-1);
                                
                                //convert the string into a json object
                                JSONObject jsonObjRecv = new JSONObject(resultString);
                                Log.i(TAG,"<JSONObject>\n"+jsonObjRecv.toString()+"\n</JSONObject>");

                                return jsonObjRecv;
                        } 

                }
                catch (Exception e)
                {
                        e.printStackTrace();
                }
                return null;
        }


        private static String convertStreamToString(InputStream is) {
        	
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();

                String line = null;
                try {
                        while ((line = reader.readLine()) != null) {
                                sb.append(line + "\n");
                        }
                } catch (IOException e) {
                        e.printStackTrace();
                } finally {
                        try {
                                is.close();
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }
                return sb.toString();
        }

}