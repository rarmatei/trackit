package com.trackit.main;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.trackit.main.R;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
 
public class UserFragment extends Fragment {
    
	ImageView imgButton;
	
    public UserFragment(){}
     
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
  
        View rootView = inflater.inflate(R.layout.fragment_user, container, false);
        return rootView;
    }
    
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
       super.onActivityCreated(savedInstanceState);
       ImageButton imgButton = (ImageButton)getView().findViewById(R.id.imgButton);
       
	   imgButton.setOnClickListener(new Button.OnClickListener() {
		   @Override
		   public void onClick(View v) {
			   //choose picture from gallery
			   Intent pickPhoto = new Intent(Intent.ACTION_PICK,
			   android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			   startActivityForResult(pickPhoto , 1);//one can be replaced with any action code
		   }
	   });
	}
	
	public void onnActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 
		switch(requestCode) { 
		case 1:
		    if(resultCode == Activity.RESULT_OK){  
		        Uri selectedImage = imageReturnedIntent.getData();
		        Bitmap bitmap = null;
				try {
					bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        imgButton.setImageBitmap(bitmap);
		    }
		break;
		}
	}
}