package com.trackit.main;
 
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.trackit.main.R;
@SuppressLint("NewApi")
public class HomeFragment extends Fragment implements GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener,LocationListener{
	private MapFragment fragment;
	private GoogleMap map;
	private HashMap<Integer, Marker> markerMap = new HashMap<Integer, Marker>();
	protected static String mPhoneNumber;
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	private LocationRequest mLocationRequest;
	private Location mCurrentLocation;
	private LocationClient mLocationClient;
	private boolean paused;
	private Thread myThread;
	
	
	//private LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
	
	/**LocationListener locationListener = new LocationListener() {
	    public void onLocationChanged(Location location) {
	      // Called when a new location is found by the network location provider.
	      //makeUseOfNewLocation(location);
	    }
	    public void onStatusChanged(String provider, int status, Bundle extras) {}
	    public void onProviderEnabled(String provider) {}
	    public void onProviderDisabled(String provider) {}
	  };*/
	  
	 @Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paused=false;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_home, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		android.app.FragmentManager fm = getFragmentManager();
		fragment = (MapFragment) fm.findFragmentById(R.id.map);
		if (fragment == null) {
			fragment = MapFragment.newInstance();
			fm.beginTransaction().replace(R.id.map, fragment).commit();
			 
		}
		if(!paused){
			if (map == null) {
				map = fragment.getMap();
				map.setMyLocationEnabled(true);
				if(servicesConnected()) {
		        	
		        	mLocationClient = new LocationClient(MainActivity.c, this, this); 
		        	mLocationClient.connect();
	
		        	updateMap();
		        	runPeriodically();
		    	}
		    	
			}
		}	
		//set thread policy to strict mode
        //this is so it will allow us to run HTTP request on the UI thread
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
	}
	 @Override
	public void onPause() {
		super.onPause();
		paused = true;
		mLocationClient.disconnect();
		
	}
		 
	@Override
	public void onResume() {
		super.onResume();
		paused = false;
		
	}
	
	 @Override
	public void onStop() {
        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();
        super.onStop();
        paused = true;
    }
	 
	@Override
	public void onDisconnected() {
		// Display the connection status
        Toast.makeText(getActivity(), "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
		
	}
	
	private void runPeriodically() {
    	(myThread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while (!paused)
                    try
                    {
                    	getActivity().runOnUiThread(new Runnable() 
                        {
                            @Override
                            public void run()
                            {
                              if(mCurrentLocation!=null) {
                            	  onLocationChanged(mLocationClient.getLastLocation());
                              	}
                               //updateMap();
                            }
                        });
                        Thread.sleep(10000);
                    }
                    catch (InterruptedException e)
                    {
                        
                    }
            }
        })).start(); 
    }
	@Override
	public void onConnected(Bundle arg0) {
		// Display the connection status
        Toast.makeText(getActivity(), "Connected", Toast.LENGTH_SHORT).show();
        mCurrentLocation = mLocationClient.getLastLocation();
    	if(mCurrentLocation!=null) {
        	LatLng currentLoc = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        	
        	map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLoc, 16));
        	//onLocationChanged(mCurrentLocation);
       }
	}
	
	@Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment f = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (f != null){ 
            getFragmentManager().beginTransaction().remove(f).commit();
        }
    }
	
	/*
     * Runs everytime the user changes his location.
     * Sends updated user information to the web service
     */
    @Override
	public void onLocationChanged(Location location) {
        // Report to the UI that the location was updated
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        
        double lat = location.getLatitude();
    	double lng = location.getLongitude();
    	
    	//Toast.makeText(getActivity(),"Location " + lat + ","+lng,Toast.LENGTH_LONG).show();
    	
    	JSONObject jsonObjSend = new JSONObject();
        try {
                //Because the phone number has the country code in front
        	    //We first must parse the string
        		if(mPhoneNumber.charAt(0)=='+'){
        			mPhoneNumber = "0"+mPhoneNumber.substring(3, mPhoneNumber.length()-1);
        		}
        		//Add the phone number, latitude and longitude to the JSON object
                jsonObjSend.put("PhoneNo", mPhoneNumber); 
                jsonObjSend.put("Latitude",lat);
                jsonObjSend.put("Longitude", lng);
                // Add a nested JSONObject (e.g. for header information)
                JSONObject header = new JSONObject();
                header.put("deviceType","Android"); // Device type
                header.put("language", "en-gb");        // Language of the Android client
                jsonObjSend.put("header", header);
                
        } catch (JSONException e) {
                e.printStackTrace();
        }
        // Send the HttpPostRequest and receive a JSONObject in return
        JSONObject jsonObjRecv = MyHttpClient.SendHttpPost("http://trackitwebapi.apphb.com/api/user/post", jsonObjSend);
        updateMap();
    }
    
   /** public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start_screen, menu);
        return true;
    }*/
    
	
	private void updateMap() {
    	//get the json array from the web api
    	JSONArray jsonArr = httpRequest.returnJson("http://trackitwebapi.apphb.com/api/user/get");
    	 for(int i = 0 ; i < jsonArr.length(); i++){
    	        try {
					JSONObject json = new JSONObject(jsonArr.getString(i));
					//parse the properties of the json object
					double lat = json.getDouble("Latitude");
			    	double lng = json.getDouble("Longitude");
			    	LatLng coordinate = new LatLng(lat, lng);
					int phoneNo = (int) json.getDouble("PhoneNo");
					String updateTime = json.getString("UpdateTime");
					int mynr = Integer.parseInt(mPhoneNumber);
			    	
					//we keep track of all the map markers in a hash table
					//if the map already contains a marker for a specific user
					//we just update its information, instead of creating a new one
					if(!markerMap.containsKey((int) json.getDouble("ID")))
				     {
				         //Add the Marker to the Map and keep track of it 
				        if(mynr==(int) json.getDouble("PhoneNo")){
				        	///TESTING!!!!!!!
				        	/*markerMap.put((int) json.getDouble("ID"), map.addMarker(new MarkerOptions()
					     	.position(coordinate)
					    	.title(phoneNo+"   "+updateTime)
					    	.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))));*/
				        }
				        else{ 
						markerMap.put((int) json.getDouble("ID"), map.addMarker(new MarkerOptions()
				     	.position(coordinate)
				    	.title(phoneNo+"   "+updateTime)
				    	.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))));
						}
				     }
					else {
						 	markerMap.get((int) json.getDouble("ID")).setPosition(coordinate);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				} 
    	    }
    }
	
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		/*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        getActivity(),
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                
                // * Thrown if Google Play services canceled the original
                // * PendingIntent
                 
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
           
             //* If no resolution is available, display a dialog to the
             //* user with the error.
             
            showErrorDialog(connectionResult.getErrorCode());
        }
		
	}
	/*
     * Called when the Activity becomes visible.
     */
    
	
    
	
	public boolean showErrorDialog(int errorCode) {
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(getActivity());
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            // Continue
            return true;
        // Google Play services was not available for some reason
        } else {
                Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode,getActivity(),
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                // If Google Play services can provide an error dialog
                if (errorDialog != null) {
                            // Create a new DialogFragment for the error dialog
                            ErrorDialogFragment errorFragment =  new ErrorDialogFragment();
                            // Set the dialog in the DialogFragment
                            errorFragment.setDialog(errorDialog);
                            // Show the error dialog in the DialogFragment
                            errorFragment.show(getFragmentManager(), "Location Updates");
                            } return false;
                }
            }
	
	
	public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;
        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }
        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }
        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
    /*
     * Handle results returned to the FragmentActivity
     * by Google Play services
     */
    @Override
	public void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
           
                switch (resultCode) {
                    case Activity.RESULT_OK :
                    
                    break;
                }
        }
    }
    
   public boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(getActivity());
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
        // Google Play services was not available for some reason
        } else {
            // Get the error code
        	Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode,
                    getActivity(),
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment =
                        new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                errorFragment.show(getFragmentManager(), "Location Updates");
                
            }
            return false;
        }
    }
	
	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}
}