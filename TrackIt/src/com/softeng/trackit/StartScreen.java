package com.softeng.trackit;
import java.util.HashMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import android.provider.Settings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;  
import org.apache.http.client.HttpClient;  import org.apache.http.impl.client.DefaultHttpClient; 

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.provider.Settings;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

//import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.*;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.softeng.trackit.Repository.httpRequests;

public class StartScreen extends FragmentActivity implements GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener,LocationListener {
	private GoogleMap mMap;
	private Location location;
	private LocationManager locationManager;
	private String provider;
	protected static String mPhoneNumber;
	private HashMap<Integer, Marker> markerMap = new HashMap<Integer, Marker>();
	
	private final static int
    CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	LocationClient mLocationClient;
	Location mCurrentLocation;
	LocationRequest mLocationRequest;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);
        
        //set thread policy to strict mode
        //this is so it will allow us to run HTTP request on the UI thread
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        //get user's telephone number
       // TelephonyManager tMgr =(TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
      //  mPhoneNumber = tMgr.getLine1Number();
        //if(mPhoneNumber.equals("")) mPhoneNumber = " ";
        //check if GPS is enabled
        checkGPS();
      //get the UI map object
        mMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
    	if(servicesConnected()) {
        	updateMap();
        	mLocationClient = new LocationClient(this, this, this);
        	mLocationClient.connect();
    	}
        
        //retrieve the best location provider
//       locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
//        Criteria criteria = new Criteria();
//        provider = locationManager.getBestProvider(criteria, false);
//        //get the last known location of the user
        //Location location = locationManager.getLastKnownLocation(provider);
//        
//        //once a location is found, show the user his coordinates
//        if(location!=null) {
//        	Toast.makeText(this, "Selected Provider "+provider, Toast.LENGTH_SHORT).show();
//        	//this function runs everytime the user changes his location
//            onLocationChanged(location);
//        }
    	
//        CHANGE THIS TO USE MAP REPO
//    	LatLng Abdn = new LatLng(57.158471, -2.096567);
//    
 //  	mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Abdn, 13));
   	mMap.setMyLocationEnabled(true);
//    	centerMapOnMyLocation();
    		
    	runPeriodically();
    	}
    
    /*
     * Called when the Activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
        mLocationClient.connect();
    }
    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();
        super.onStop();
    }
    
  //check if GPS is enabled
    private boolean checkGPS() {
    	LocationManager service = (LocationManager)getSystemService(LOCATION_SERVICE);
        boolean enabledGPS = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
        
        //if GPS is not enabled, show the user a message
        //and open the settings page
        if(!enabledGPS) {
        	Toast.makeText(this, "GPS singal not found", Toast.LENGTH_LONG).show();
        	Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        	startActivity(intent);
        	
        }
        return enabledGPS;
    }
    
    /*
     * Runs everytime the user changes his location.
     * Sends updated user information to the web service
     */
    @Override
    public void onLocationChanged(Location location) {
        // Report to the UI that the location was updated
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        
        double lat = location.getLatitude();
    	double lng = location.getLongitude();
    	
    	Toast.makeText(this,"Location " + lat + ","+lng,Toast.LENGTH_LONG).show();
    	
    	JSONObject jsonObjSend = new JSONObject();

        try {
                //Because the phone number has the country code in front
        	    //We first must parse the string
        		if(mPhoneNumber.charAt(0)=='+'){
        			mPhoneNumber = "0"+mPhoneNumber.substring(3, mPhoneNumber.length()-1);
        		}
        		//Add the phone number, latitude and longitude to the JSON object
                jsonObjSend.put("PhoneNo", mPhoneNumber); 
                jsonObjSend.put("Latitude",lat);
                jsonObjSend.put("Longitude", lng);

                // Add a nested JSONObject (e.g. for header information)
                JSONObject header = new JSONObject();
                header.put("deviceType","Android"); // Device type
                header.put("language", "en-gb");        // Language of the Android client
                jsonObjSend.put("header", header);
                

        } catch (JSONException e) {
                e.printStackTrace();
        }

        // Send the HttpPostRequest and receive a JSONObject in return
        JSONObject jsonObjRecv = MyHttpClient.SendHttpPost("http://trackitwebapi.apphb.com/api/user/", jsonObjSend);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start_screen, menu);
        return true;
    }
    
    
    
    /*
     * Gets a JSON array of all the users from the web service
     * Updates the pins on the map
     * Will be called every 20 seconds
     */
    private void updateMap() {
    	//get the json array from the web api
    	JSONArray jsonArr = httpRequests.returnJson("http://trackitwebapi.apphb.com/api/user/");
    	 for(int i = 0 ; i < jsonArr.length(); i++){
    	        try {
					JSONObject json = new JSONObject(jsonArr.getString(i));
					//parse the properties of the json object
					double lat = json.getDouble("Latitude");
			    	double lng = json.getDouble("Longitude");
			    	LatLng coordinate = new LatLng(lat, lng);
					int phoneNo = (int) json.getDouble("PhoneNo");
					String updateTime = json.getString("UpdateTime");
			    	
					//we keep track of all the map markers in a hash table
					//if the map already contains a marker for a specific user
					//we just update its information, instead of creating a new one
					if(!markerMap.containsKey(json.getDouble("ID")))
				     {
				         //Add the Marker to the Map and keep track of it 
				         this.markerMap.put((int) json.getDouble("ID"), this.mMap.addMarker(new MarkerOptions()
				     	.position(coordinate)
				    	.title(phoneNo+"   "+updateTime)));
				     }
					else {
						//update an existing pin
						markerMap.get(json.getDouble("ID")).setPosition(coordinate);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				} 
    	    }
    }
  
    /*
     * Starts a separate thread that will run the updateMap() on the main UI thread
     * Then sleep for 20 seconds
     */
    private void runPeriodically() {
    	(new Thread(new Runnable()
        {

            @Override
            public void run()
            {
                while (!Thread.interrupted())
                    try
                    {
                        runOnUiThread(new Runnable() 
                        {

                            @Override
                            public void run()
                            {
                               updateMap();
                            }
                        });
                        Thread.sleep(20000);
                    }
                    catch (InterruptedException e)
                    {
                        
                    }
            }
        })).start(); 
    }

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		/*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            showErrorDialog(connectionResult.getErrorCode());
        }
		
	}
	
	public boolean showErrorDialog(int errorCode) {
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status

            // Continue
            return true;
        // Google Play services was not available for some reason
        } else {
                Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode,this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                // If Google Play services can provide an error dialog
                if (errorDialog != null) {
                            // Create a new DialogFragment for the error dialog
                            ErrorDialogFragment errorFragment =  new ErrorDialogFragment();
                            // Set the dialog in the DialogFragment
                            errorFragment.setDialog(errorDialog);
                            // Show the error dialog in the DialogFragment
                            errorFragment.show(getFragmentManager(), "Location Updates");

                            } return false;
                }
            }

	@Override
	public void onConnected(Bundle arg0) {
		// Display the connection status
        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
        mCurrentLocation = mLocationClient.getLastLocation();
    	if(mCurrentLocation!=null) {
        	LatLng currentLoc = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        	
        	mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLoc, 14));
       }
	}

	@Override
	public void onDisconnected() {
		// Display the connection status
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
		
	}
	
	public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;
        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }
        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }
        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
    /*
     * Handle results returned to the FragmentActivity
     * by Google Play services
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK :
                    /*
                     * Try the request again
                     */
                    break;
                }
        }
     }
    public boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
        // Google Play services was not available for some reason
        } else {
            // Get the error code
        	Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode,
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment =
                        new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                errorFragment.show(getFragmentManager(), "Location Updates");
                
            }
            return false;
        }
    }
	
	
    
}

