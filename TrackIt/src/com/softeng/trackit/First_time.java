package com.softeng.trackit;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.SharedPreferences;

public class First_time extends Activity {

	final String PREFS_NAME = "TrackPrefsFile";
	String number;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
    	
		// Restore any saved state 
		super.onCreate(savedInstanceState);
        
		// Set content view
        setContentView(R.layout.first_time_screen);
        
        // Initialize UI elements
		final EditText numText = (EditText) findViewById(R.id.number);
		final Button button = (Button) findViewById(R.id.sendButton);
        
		final SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

		//UN-COMMENT THIS, ONLY FOR DEMOING PURPOSES
		settings.edit().putBoolean("my_first_time", true).commit(); 
		//UN-COMMENT
		
		
		
        if (settings.getBoolean("my_first_time", true)) {
            //the app is being launched for first time, do something        
            Log.d("Comments", "First time");

            // first time task
            
    		button.setOnClickListener(new Button.OnClickListener() {
    			@Override
    			public void onClick(View v) {   				
    			String number =  numText.getText().toString();	
    			// i guess this will have to be changed and store the number somewhere else
    			StartScreen.mPhoneNumber = number; 
                settings.edit().putString("phoneNo", number).commit();
                //TO-DOS
                //need to check if there is a user with this phone number
                //need to move my_first_time to Register page if the user is not registered

                // record the fact that the app has been started at least once
                settings.edit().putBoolean("my_first_time", false).commit(); 
				Intent mapIntent = new Intent(First_time.this, Register.class);
				startActivity(mapIntent);
    			}
    		});
            
            
            
        }
        else {
        	StartScreen.mPhoneNumber = settings.getString("phoneNo", " "); 
        	Intent mapIntent = new Intent(First_time.this, StartScreen.class);
			startActivity(mapIntent);
        }
       
	}
}
