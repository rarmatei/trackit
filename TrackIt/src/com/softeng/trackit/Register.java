package com.softeng.trackit;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Register extends Activity {

	ImageView imageView;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    setContentView(R.layout.register_screen);
	    
	    final EditText emailText = (EditText) findViewById(R.id.email);
	    final EditText name = (EditText) findViewById(R.id.firstLastName);
	    final Button registerButton = (Button) findViewById(R.id.registerBtn);
	    final Button galleryBtn = (Button) findViewById(R.id.openGalleryBtn);
	    final Button cameraBtn = (Button) findViewById(R.id.openCameraBtn);
	    imageView = (ImageView) findViewById(R.id.imageView);
	    registerButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {   				
			String userName =  name.getText().toString();	
			String email =  emailText.getText().toString();
			//need to send these to the DB before showing the map
			//store them in phone settings as well
			
			
			
			
			Intent mapIntent = new Intent(Register.this, StartScreen.class);
			startActivity(mapIntent);
			
			
			
			}
		});
	    
	    galleryBtn.setOnClickListener(new Button.OnClickListener() {
	    	@Override
	    	public void onClick(View v) {
	    		//choose picture from gallery
				Intent pickPhoto = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(pickPhoto , 1);//one can be replaced with any action code
	    	}
	    });
	    
	    cameraBtn.setOnClickListener(new Button.OnClickListener() {
	    	@Override
	    	public void onClick(View v) {
	    		//take picture with camera
				Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(takePicture, 0);//zero can be replaced with any action code
	    	}
	    });
	    
	    
	    
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 
		switch(requestCode) {
		case 0:
		    if(resultCode == RESULT_OK){  
		        Uri selectedImage = imageReturnedIntent.getData();
		        imageView.setImageURI(selectedImage);
		    }

		break; 
		case 1:
		    if(resultCode == RESULT_OK){  
		        Uri selectedImage = imageReturnedIntent.getData();
		        imageView.setImageURI(selectedImage);
		    }
		break;
		}
		}

	
	
}
