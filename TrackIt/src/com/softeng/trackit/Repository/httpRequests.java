package com.softeng.trackit.Repository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

public abstract class httpRequests {

	/*
     * Performs an HTTP GET request to the web service and returns a JSON array.
     */
    public static JSONArray returnJson(String url) {
    	StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
          //executes and returns an http response from the URL
          HttpResponse response = client.execute(httpGet);
          
            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(content));
            String line;
            //if the returned string contains multiple json arrays add them all to the builder
            while ((line = reader.readLine()) != null) {
              builder.append(line);
            }
         
        } catch (ClientProtocolException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
        
        JSONArray jsonArray = null;
		try {
			//create the final json array
			jsonArray = new JSONArray(builder.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
        return jsonArray;
    }
	
}
